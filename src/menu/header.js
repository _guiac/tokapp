import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
//import Icon from 'react-native-vector-icons/FontAwesome';
//import {Drawer, Container, Header, Content,Button } from 'native-base';


export default class Header extends Component {
    render() {
        return (

            <View style={styles.container}>
                <Text style={styles.txtHeader}>
                    Conteúdo header
                </Text>
            </View>
            // <View style={styles.header}>
            //     <Text style={styles.txtHeader}>LUMENX</Text>
            // </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'dodgerblue',
        justifyContent: 'center',
        alignItems: 'center',
        height: 70,
    },
    txtHeader: {
        fontSize: 30,
        color:'#fff',
        fontWeight: "100",
    }
})