import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity,View} from 'react-native';
import axios from 'axios'

export default class SimpleToggleButton extends Component {

    state ={
        toggle:false,
        value:""
    }
    

    _onPress(){
        const newState = !this.state.toggle;
        this.setState({toggle:newState});
        this.props.onStateChange && this.props.onStateChange(newState);
        //const value = JSON.stringify(newState);
    }

    render() {
        const {toggle} = this.state;
        const textValue = toggle?"ON":"OFF";
        const buttonBg = toggle?"#62b1f6":"white"; //dodgerblue
        const textColor = toggle?"white":"#62b1f6";
                
        return (
            <TouchableOpacity
                style={{
                    height: 60,
                    width: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderColor: "#62b1f6",
                    borderWidth: 2,
                    backgroundColor: buttonBg
                }}
                onPress={() => this._onPress()}>
                <Text style={{ fontSize: 20, color: textColor }}>{textValue}</Text >
            </ TouchableOpacity >

        )
    }
}
