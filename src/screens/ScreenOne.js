///////////////// WifiDaemon.js

import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  PermissionsAndroid,
  TouchableNativeFeedback,
  Modal,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image
} from 'react-native';
import Net from 'react-native-tcp';
import { createSocket } from 'react-native-udp';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';
// import { NetworkInfo } from 'react-native-network-info'; // Did not use, but could be useful lib
import {
  updateItem,
  size,
  removeItem,
  hasItem,
  toByteArray,
  multicastIP,
  multicastPort
} from '../Lib/Utilities';
import { receiveCommand, sendCommand } from '../Models/Device';
import SimpleToggleButton from '../common/SimpleToggleButton';


export default class WifiDaemon extends Component {
  constructor() {
    super();
    this.availableDevices = {};
    this.chatter = [];
    this.multicastClient = null;
    this.deviceConnections = {};
    this.deviceAckTimes = {};
    this.pingTimer = null;
    this.updateChatter = this.updateChatter.bind(this);
    this._onStateChange = this._onStateChange.bind(this);
  }
  static navigationOptions = { title: 'Welcome', header: null };

  updateChatter(msg) {
    this.chatter = this.chatter.concat([msg]);
  }

  sendMessage(msg) {
    console.log(msg);//msg = 'd' --- { '0': 100 }
    console.log('Não está enviando o pacote para a rede');
    this.multicastClient.send(msg, 0, msg.length, multicastPort, multicastIP, function(err) {
      //{ '0': 100 }, 1, 4210, '239.1.1.10'
      if (err) throw err;
      console.log('Multicast sent: ', msg);
    });
  }

  scan() {
    const buf = toByteArray('d');
    console.log('SCAN FOR DISCOVERY');
    console.log(buf, buf.length, multicastPort, multicastIP);
    this.sendMessage(buf);
  }

  startMulticast() {
    if (this.multicastClient) {
      console.log('Multicast already started...');
      this.scan();
      return this.multicastClient;
    }
    this.multicastClient = createSocket('udp4');
    this.multicastClient.on('listening', (err) => {
      if (err) throw err;
      this.multicastClient.setBroadcast(true);
      this.multicastClient.addMembership(multicastIP);
      console.log('teste');
      this.scan();
    });
    
    this.multicastClient.on('message', (data, info) => {
      const dataString = String(data);
      const msg = dataString.trim().split(',');
      const evt = msg[0][0];
      // console.tron.log('NEW EVENT: ' + data);
      
      if (evt === 'D') {
        const ip = msg[1];
        const lane = msg[2];
        const deviceId = msg[3];
        // console.log('New Event:', evt, ip, lane, dataString, JSON.stringify(info.address));
        info['name'] = 'Lane ' + lane;
        info['ip'] = ip;
        info['deviceID'] = deviceId;
        // console.log("Device Discovered: ", ip)
        if (!hasItem(this.availableDevices, deviceId)) {
          this.availableDevices = updateItem(this.availableDevices, deviceId, info);
        }
      }
    });

    this.multicastClient.bind(multicastPort);
    return this.multicastClient;
  }


  startDeviceConnection(id, ip, port) {
    // console.tron.log('startDeviceConnection: Connecting to ' + id + ', ' + ip + ':' + port);
    if (!hasItem(this.availableDevices, id)) {
      // console.tron.log('startDeviceConnection: Device is not available');
    }
    if (hasItem(this.deviceConnections, id)) {
      // console.tron.log('startDeviceConnection: Already connected...reconnecting');
      this.destroyDeviceConnection(id);
    }
    const client = Net.createConnection({ host: ip, port: port }, () => {
      // console.tron.log('startDeviceConnection: Server connected on ' + ip);
    });

    client.setTimeout(3000);

    this.deviceConnections[id] = client;
    this.deviceAckTimes[id] = null;
    return client;
  }

  destroyDeviceConnection(id) {
    if (hasItem(this.deviceConnections, id)) {
      this.deviceConnections[id].destroy();
      delete this.deviceConnections[id];
    }
    if (hasItem(this.deviceAckTimes, id)) {
      delete this.deviceAckTimes[id];
    }
  }

  changeName = () => {
    this.props.navigation.navigate('ScreenTwo');
  }
  _onStateChange(newState) {
    const value = newState ? 'ON' : 'OFF';
    this.setState({ toggleText: value }); //ALTERAR O ESTADO DO COMPONENTE
    // console.log("toggleText");
    axios.get(`http://192.168.0.100/deviceValue?value=${value}`)// { value: value })
      .then(response => { (console.log(response.data)) })//e aí??? fazer oq com a resposta?
      .catch(() => { console.log('Error'); })
  }

  render() {
    //const {deviceList, name} = this.props;
    const { navigation } = this.props;
    const name = navigation.getParam('name', 'Default');
    return (
      <View style={styles.mainContainer}>

        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
          <Icon name="home" size={60} color="#3F51B5" />
        </View>

        <TouchableOpacity style={styles.btn}
          onPress={() => this.startMulticast()}>
          <Text style={styles.txtScan}>Scan</Text>
        </TouchableOpacity> 


        {/* construir um forma para randerizar somente os ip's válidos */}
        <View style={{ flex: 6 }}>
          <ScrollView>
            <View style={styles.deviceBox}>
              <TouchableHighlight //Tem que passar o ip para poder dar o post
                style={styles.btnDevice}
                onPress={() => this.changeName()}>
                <Text style={styles.txt}>{name}</Text>
              </TouchableHighlight>
              <View style={styles.tabbutton}>
                <SimpleToggleButton onStateChange={this._onStateChange} />
              </View>
            </View>

            <View style={styles.deviceBox}>
              <TouchableHighlight //Tem que passar o ip para poder dar o post
                style={styles.btnDevice}
                onPress={() => this.changeName()}>
                <Text style={styles.txt}>{name}</Text>
              </TouchableHighlight>
              <View style={styles.tabbutton}>
                <SimpleToggleButton onStateChange={this._onStateChange} />
              </View>
            </View>

            <View style={styles.deviceBox}>
              <TouchableHighlight //Tem que passar o ip para poder dar o post
                style={styles.btnDevice}
                onPress={() => this.changeName()}>
                <Text style={styles.txt}>{name}</Text>
              </TouchableHighlight>
              <View style={styles.tabbutton}>
                <SimpleToggleButton onStateChange={this._onStateChange} />
              </View>
            </View>

            <View style={styles.deviceBox}>
              <TouchableHighlight //Tem que passar o ip para poder dar o post
                style={styles.btnDevice}
                onPress={() => this.changeName()}>
                <Text style={styles.txt}>{name}</Text>
              </TouchableHighlight>
              <View style={styles.tabbutton}>
                <SimpleToggleButton onStateChange={this._onStateChange} />
              </View>
            </View>

            <View style={styles.deviceBox}>
              <TouchableHighlight //Tem que passar o ip para poder dar o post
                style={styles.btnDevice}
                onPress={() => this.changeName()}>
                <Text style={styles.txt}>{name}</Text>
              </TouchableHighlight>
              <View style={styles.tabbutton}>
                <SimpleToggleButton onStateChange={this._onStateChange} />
              </View>
            </View>
          </ScrollView>
        </View>
        <View style={styles.img}>
          <Image style={{ width: 80, height: 33 }}
            source={require('../img/lumenx.png')} />
        </View>
      </View>

    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'whitesmoke',
  },
  btn: {
    height: 40,
    // width: 350,
    marginTop: 0,
    marginLeft: 30,
    marginRight: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3F51B5',
    paddingTop: 30,
    paddingBottom: 30,
    borderRadius: 10,
  },
  txtScan: {
    fontSize: 25,
    color: '#fff',
    fontWeight: "100",
  },
  deviceBox: {
    marginTop: 10,
    marginLeft: 30,
    marginRight: 30,
    height: 100,
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 10,
    borderWidth: 2,
    borderColor: "#ccc",
  },
  txt: {
    fontSize: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnDevice: {
    margin: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabbutton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: 'whitesmoke',
  },
  img: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  }
});
