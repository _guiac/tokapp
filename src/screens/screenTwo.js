import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableHighlight } from 'react-native';
import axios from 'axios';

//import Header from '../header/header'

export default class ScreenTwo extends Component {
    static navigationOptions = { title: 'Welcome', header: null };

    constructor(props){
        super(props);
        this.state = { name: '', newname: '' };
    }
    _onPress(name){
        console.log(name);
        axios.get(`http://192.168.0.103/deviceName?name=${name}`)//, { name: "name" })
        .then(response => { (console.log(response.data));})//e aí??? fazer oq com a resposta?
        .catch(() => { console.log('Error'); });
        
        this.props.navigation.navigate('WifiDaemon', { name });
        // axios.post('http://192.168.0.102/deviceName', {name:newname})
        // .then(response => { this.setState({ status: response.data }); })//e aí??? fazer oq com a resposta?
        // .catch(() => { console.log('Error') })
    }

    _back = () => {
        this.props.navigation.navigate('WifiDaemon');
    };

    render() {
        return (
            <View style={styles.container}>
                
                <View style={styles.containerBody}>
                    <Text style={styles.txtTittle}>Alterar Nome</Text>
                    <TextInput 
                        placeholder="Nome do dispositivo.."
                        style={styles.txtInput}
                        maxLength={20}
                        onChangeText={(typedText) => this.setState({ name: typedText })} 
                    />

                    <View style={{ flexDirection: 'row' }}>

                        <TouchableHighlight 
                            style={styles.btn}
                            onPress={() => this._onPress(this.state.name)}
                        >
                            <Text style={styles.txtBtn}>Alterar</Text>
                        </TouchableHighlight>

                        <TouchableHighlight 
                            style={styles.btn}
                            onPress={() => this._back()}
                        >
                            <Text style={styles.txtBtn}>Cancelar</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    containerBody: {
        flex: 8,
        alignItems: 'center',
        backgroundColor: '#fff',
        justifyContent: 'center',
    },
    txtTittle: {
        fontSize: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    txtInput: {
        margin: 40,
        padding: 10,
        fontSize: 18,
        width: 300
    },
    txtBtn: {
        fontSize: 22,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btn: {
        margin: 20,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});
