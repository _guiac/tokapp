/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Drawer, Container, Header, Content, Button } from 'native-base';

import ScreenTwo from './src/screens/screenTwo';
// import Header from './src/menu/header';
import ScreenOne from './src/screens/ScreenOne';
import SideBar from './src/menu/sideBar'


export default class App extends Component {
  closeDrawer = () => {
    this.drawer._root.close()
  };
  openDrawer = () => {
    this.drawer._root.open()
  };
  render() {
    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<SideBar navigator={this.navigator} />}
        onClose={() => this.closeDrawer()}>
        <Container>
          <Header>
            <Container
              style={{
                flex: 1,
                flexDirection: 'row',
                backgroundColor: '#3F51B5',
                justifyContent: 'space-between',
                paddingTop: 12,
                // paddingTop: 12,
                // paddingLeft: 12,
                // alignItems: 'center'
              }}>
              <Icon onPress={() => this.openDrawer()}
                name="bars"
                size={30}
                color="#fff"
                style={{
                // paddingTop: 12
                }} />
              <Image style={{ width: 70, height: 30 }}
                source={require('./src//img/logo-tok.png')} />
              {/* <Text style={[styles.txt, {color: '#3F51B5'}]}>.</Text> */}
              <Icon name="bars" size={30} color="#3F51B5" />
            </Container>
          </Header>

          <RootStack />

        </Container>
      </Drawer>
    );
  }
}

const RootStack = createStackNavigator(
  {
    // ScreenOne: ScreenOne,
    ScreenOne: ScreenOne,
    ScreenTwo: ScreenTwo,
    // AutoConnect: AutoConnect,
    //UdpSockets: UdpSockets,

  },
  {
    initialRouteName: 'ScreenOne',
  },
);
const styles = StyleSheet.create({
  txt: {
    fontSize: 25,
    color: '#fff',
    paddingTop: 10,
    fontWeight: "100",
  },
});
